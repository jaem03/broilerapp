import styled from 'styled-components/native';
import {View , Text , Image, TextInput, Touchable} from 'react-native';
import Constants from "expo-constants";

const StatusBarHeight = Constants.StatusBarHeight;
//colors
export const Colors = {
    primary: '#ffffff',
    secondary: '#E5D7EB',
    tertiary: '#1F2937',
    darkLight: '#9CA3AF',
    brand : '#630A10',
    green: '#10B981',
    red: '#EF4444',
};
const {primary , secondary, tertiary , darkLight , brand , green , red} = Colors;

export const StyledContainer = styled.View`
    flex: 1;
    padding: 25px;
    padding-top : ${StatusBarHeight}px;
    background-color: ${primary};
`
export const InnerContainer = styled.View`
    flex: 1;
    width: 100%;
    align-items :center;
`

export const DashboardContainer = styled(InnerContainer)`
    padding: 25px;
    padding-top: 10px;
    justifyContent: center;

`;


export const PageLogo = styled.Image`
    width: 250px;
    height : 200px;
`

export const PageTitle = styled.Text`
    font-size: 30px;
    text-align : center;
    font-weight : bold;
    color: ${brand};
    padding: 10px;
`

export const Subtitle = styled.Text`
    font-size :18px;
    margin-bottom: 20px;
    letter-spacing: 1px;
    font-weight: bold;
    color: ${tertiary};

`
export const StyledFormArea = styled.View`
    width: 90%;
`
export const StyledTextInput = styled.TextInput`
    background-color: ${secondary};
    padding : 15px;
    padding-left: 55px;
    padding-right: 55px;
    border-radius: 5px;
    font-size : 16px;
    height : 60px;
    margin-vertical: 3px;
    margin-bottom: 10px;
    color: ${tertiary};
`
export const StyledInputLabel = styled.Text`
    color: ${tertiary};
    font-size: 13px;
    text-align: left;
`
export const LeftIcon = styled.View`
    left: 15px;
    top: 38px;
    position: absolute;
    z-index: 1;
`
export const RigthIcon = styled.TouchableOpacity`
    right: 15px;
    top: 38px;
    position: absolute;
    z-index: 1;
`
export const StyledButton = styled.TouchableOpacity`
    padding: 15px;
    background-color: ${brand};
    justifyContent : center;
    align-items :center;
    border-radius: 5px;
    margin-vertical: 5px;
    height: 60px;
`
export const ButtonText = styled.Text`
    color: ${primary};
    font-size : 16px;
`
export const MsgBox = styled.Text`
    text-align: center;
    font-size: 13px;
`
export const Line = styled.View`
    height: 1px;
    width: 100%;
    background-color: ${darkLight};
    margin-vertical: 10px;

`

export const ExtraView = styled.View`
    justifyContent : center;
    flex-direction: row;
    alignItems: center;
    padding: 10px;
`
export const ExtraText = styled.Text`
    justifyContent: center;
    alignItems: center;
    color: ${tertiary};
    font-size: 15px;
`
export const Avatar = styled.Image`
    width: 100px;
    height: 100px;
    margin: auto;
    border-radius: 50px;
    border-width: 2px;
    border-color: ${secondary};
    margin-bottom: 10px;
    margin-top: 10px;

`