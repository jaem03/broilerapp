import React , {useState, useEffect, createRef} from 'react';
import {Pressable , View,StyleSheet,  Text, Image, FlatList , Keyboard, ActivityIndicator,TouchableOpacity , ScrollView, KeyboardAvoidingView , TextInput} from 'react-native';
import { List , FAB } from 'react-native-paper';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AsyncStorage from '@react-native-community/async-storage';

function Production({navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getBuildings = async () => {
     try {
      const response = await fetch('http://192.168.1.103/broiler/get-buildings');
      const json = await response.json();
      setData(json);
      console.log(data);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getBuildings();
  }, []);
  
  return (
    <View style={{ flex: 1, padding: 24 }}>
    {isLoading ? <ActivityIndicator /> : (
        <FlatList

            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
              
                <List.Item
                style={styles.item}
                  onPress={() => {
                    navigation.navigate('Batches', {
                      buildingId: item.building,
                    });
                  }}
                    title= {item.building}
                    description= {"Date Added: " + item.created_at}
                    left={props => <TouchableOpacity>
                        <Image
                            style={{ width: 50, height: 50 }}
                            source={require('./../../assets/building.png')}
                        />
                    </TouchableOpacity>}
                />
            )}
        />
      
  
    )}

</View>
  );
}

function Batches({ route, navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [errortext, setErrortext] = useState('');
  
  const { buildingId } = route.params;

  const getBatches = () => {
    
    setErrortext('');
    setLoading(true);
    let dataToSend = {'buildingId': buildingId};
    let formBody = [];
    
    for (let key in dataToSend) {
      let encodedKey = encodeURIComponent(key);
      let encodedValue = encodeURIComponent(dataToSend[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    fetch('http://192.168.1.103/broiler/get-batches', {
      method: 'POST',
      body: formBody,
      headers: {
        'Content-Type':
        'application/x-www-form-urlencoded;charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setLoading(false);
        setData(responseJson);
      })
      .catch((error) => {
        setLoading(false);
        console.error(error);
      });
  };

  useEffect(() => {
    getBatches();
  }, []);
  
  return (
    <View style={{ flex: 1, padding: 24 }}>
    {isLoading ? <ActivityIndicator /> : (
        <FlatList
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
                <List.Item
                style={styles.item}
                    onPress={() => {navigation.navigate('Production Record', {batchId: item.batch , buildingId: item.building}) }}
                    
                    title=  {"Batch:" + item.batch + "("+item.total_chickens+" Chickens)" }
                    description= {"Last Update: " + item.created_at}
                    
                    left={props => <TouchableOpacity>
                        <Image
                            style={{ width: 50, height: 50 }}
                            source={require('./../../assets/batch.png')}
                        />
                    </TouchableOpacity>}
                />
            )}
        />
     
  
    )}

</View>
  );
}

function BatchRecord({ route, navigation }) {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [batch, setBatch] = useState([]);
  const [building, setBuilding] = useState([]);
  const [errortext, setErrortext] = useState('');
  
  const { batchId } = route.params;
  const { buildingId } = route.params;
  const getBatches = () => {
    
    setErrortext('');
    setLoading(true);
    let dataToSend = {'batch': batchId};
    let formBody = [];
    
    for (let key in dataToSend) {
      let encodedKey = encodeURIComponent(key);
      let encodedValue = encodeURIComponent(dataToSend[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    fetch('http://192.168.1.103/broiler/get-production', {
      method: 'POST',
      body: formBody,
      headers: {
        'Content-Type':
        'application/x-www-form-urlencoded;charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        setLoading(false);
        setData(responseJson);
        setBatch(batchId);
      })
      .catch((error) => {
        setLoading(false);
        console.error(error);
      });
  };

  useEffect(() => {
    getBatches();
  }, []);
  
  return (
    <View style={{ flex: 1, padding: 24 }}>
    {isLoading ? <ActivityIndicator /> : (
        <FlatList
            data={data}
            keyExtractor={({ id }, index) => id}
            renderItem={({ item }) => (
                <List.Item
                style={styles.item}
                    
                    title=  {"Date: " + item.date}
                    description= {"Harvested ("+item.harvested_chickens+")\nMortality("+item.mortality +")"}
                    
                    left={props => <TouchableOpacity>
                        <Image
                            style={{ width: 50, height: 50 }}
                            source={require('./../../assets/check.png')}
                        />
                    </TouchableOpacity>}
                />
            )}
        />
    )}
  <FAB
    style={styles.fab}
    large
    icon="plus"
    onPress={() => {navigation.navigate('Add Record', {batch : batchId , building: buildingId}) }}
  />
</View>
  );
}

function AddRecord({ route, navigation }) {
  const [loading, setLoading] = useState(false);
  const [mortalityErrortext, setMortalityErrortext] = useState('');
  const [feedErrortext, setFeedErrortext] = useState('');
  const [remarksErrortext, setRemarksErrortext] = useState('');

  const [mortality , setMortality] = useState('');
  const [feed , setFeed] = useState('');
  const [remarks , setRemarks] = useState('');
  const [harvested , setHarvested] = useState('');
  const [worker , setWorker] = useState('');

  

  const remarksInputRef = createRef();
  const feedInputRef = createRef();
  const harvestedInputRef = createRef();

  const worker_name = AsyncStorage.getItem('worker_name');
  console.log(worker_name);
  
  const {batch} = route.params;
  const {building} = route.params;

  const handleSubmitPress = () => {
    setFeedErrortext('');
    setMortalityErrortext('');
    setRemarksErrortext('');
    if (!mortality) {
      setMortalityErrortext('Please enter mortality');
      return;
    }
    if (!feed) {
      setFeedErrortext('Please enter feed consumption.');
      return;
    }
    if (!remarks) {
      setRemarksErrortext('Please enter remarks.');
      return;
    }
    setLoading(true);
    let dataToSend = {'worker' : worker, 'building' : building,'batch' : batch, 'mortality': mortality, 'feed': feed , 'remarks' : remarks , 'harvested_chickens' : harvested};
    let formBody = [];
    
    for (let key in dataToSend) {
      let encodedKey = encodeURIComponent(key);
      let encodedValue = encodeURIComponent(dataToSend[key]);
      formBody.push(encodedKey + '=' + encodedValue);
    }
    formBody = formBody.join('&');

    fetch('http://192.168.1.103/broiler/save-data', {
      method: 'POST',
      body: formBody,
      headers: {
   
        'Content-Type':
        'application/x-www-form-urlencoded;charset=UTF-8',
      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
    
        setLoading(false);
        if (responseJson === 0) {
          console.log("error");
        } else {

          navigation.navigate('Production Record', {batch : batch , building: building})

         
        }
      })
      .catch((error) => {

        setLoading(false);
        console.error(error);
      });
  };
  
  return (
 
    <View style={styles.mainBody}>
    <ScrollView
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={{
        flex: 1,
        justifyContent: 'center',
        alignContent: 'center',
      }}>
      <View>
        <KeyboardAvoidingView enabled>
          <View style={styles.SectionStyle}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={(Mortality) =>
                setMortality(Mortality)
              }
              placeholder="Mortality Check" 
              placeholderTextColor="#8b9cb5"
              autoCapitalize="none"
              keyboardType="numeric"
              returnKeyType="next"
              onSubmitEditing={() =>
                feedInputRef.current &&
                feedInputRef.current.focus()
              }
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
          </View>
          {mortalityErrortext != '' ? (
            <Text style={styles.errorTextStyle}>
              {mortalityErrortext}
            </Text>
          ) : null}
          <View style={styles.SectionStyle}>
          <TextInput
              style={styles.inputStyle}
              onChangeText={(Feed) =>
                setFeed(Feed)
              }
              placeholder="Feed Consumption" 
              placeholderTextColor="#8b9cb5"
              autoCapitalize="none"
              ref={feedInputRef}
              returnKeyType="next"
              onSubmitEditing={() =>
                remarksInputRef.current &&
                remarksInputRef.current.focus()
              }
              underlineColorAndroid="#f000"
              blurOnSubmit={false}

            />
          </View>
          {feedErrortext != '' ? (
            <Text style={styles.errorTextStyle}>
              {feedErrortext}
            </Text>
          ) : null}
          <View style={styles.SectionStyle}>
          <TextInput
              style={styles.inputStyle}
              onChangeText={(Harvested) =>
                setHarvested(Harvested)
              }
              placeholder="Harvested Chickens" 
              placeholderTextColor="#8b9cb5"
              autoCapitalize="none"
              ref={harvestedInputRef}
              returnKeyType="next"
              keyboardType="numeric"
              onSubmitEditing={() =>
                remarksInputRef.current &&
                remarksInputRef.current.focus()
              }
              underlineColorAndroid="#f000"
              blurOnSubmit={false}

            />
          </View>
          <View style={styles.SectionStyle}>
          <TextInput
              style={styles.inputStyle}
              onChangeText={(Remarks) =>
                setRemarks(Remarks)
              }
              placeholder="Remarks" 
              placeholderTextColor="#8b9cb5"
              autoCapitalize="none"
              ref={remarksInputRef}

              returnKeyType="next"
              onSubmitEditing={Keyboard.dismiss}
              underlineColorAndroid="#f000"
              blurOnSubmit={false}
            />
            </View>

          
         
          {remarksErrortext != '' ? (
            <Text style={styles.errorTextStyle}>
              {remarksErrortext}
            </Text>
          ) : null}
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={handleSubmitPress}>
            <Text style={styles.buttonTextStyle}>Submit</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </View>
    </ScrollView>
  </View>

  );
};


const Stack = createNativeStackNavigator();

const ProductionScreen = (props) => {

  
  return (
  
    <Stack.Navigator>
      <Stack.Screen name="Production" component={Production} />
      <Stack.Screen name="Batches" component={Batches} />
      <Stack.Screen name="Production Record" component={BatchRecord} />
      <Stack.Screen name="Add Record" component={AddRecord} />
    </Stack.Navigator>

  );
};
const styles = StyleSheet.create({
  item: {
    backgroundColor: '#c7ecee',
    padding: 20,
    borderRadius: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    alignContent: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginLeft: 35,
    marginRight: 35,
    margin: 10,
  },
  buttonStyle: {
    backgroundColor: '#c0392b',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#7DE24E',
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 20,
    marginBottom: 25,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: 'black',
    paddingLeft: 15,
    paddingRight: 15,
    borderWidth: 1,
    borderRadius: 30,
    borderColor: '#dadae8',
  },
  registerTextStyle: {
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 14,
    alignSelf: 'center',
    padding: 10,
  },
  errorTextStyle: {
    color: 'red',
    textAlign: 'center',
    fontSize: 14,
  },
});

export default ProductionScreen;

